const Films = props => {
    return React.createElement('div', {className: 'film'},
        React.createElement('img', {src: props.src}),
        React.createElement('h4', {}, `Название: ${props.name}`),
        React.createElement('p', {}, `Год: ${props.year}`),
    );
};

const block = document.getElementById('root');
const film = (
    <React.Fragment>
        <Films src='img/2.jpg' name="Гладиатор" year="2000" />
        <Films src='img/3.jpg' name="Оружейный барон" year="2005" />
        <Films src='img/1.jpg' name="28 Панфиловцев" year="2016" />
    </React.Fragment>
);
ReactDOM.render(film, block);