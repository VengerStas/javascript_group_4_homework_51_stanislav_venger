import React, { Component } from 'react';
import './App.css';
import Header from './Components/js/Header';
import Section from './Components/js/InfoBlock';
import Footer from './Components/js/Footer';

class App extends Component {
  render() {
    return (
        <React.Fragment>
          <Header />
          <Section />
          <Footer />
        </React.Fragment>
    );
  }
}

export default App;
