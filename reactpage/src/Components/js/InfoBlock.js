import React from 'react';
import photoOne from '../img/photo1.jpg';
import photoTwo from '../img/photo2.jpg';

const Section = () => {
    return (
        <section className="main-info">
            <div className="container">
                <div className="all-about  clearfix">
                    <img className="girl" src={photoOne} alt="girl" />
                        <div className="fitness-first">
                            <h6>it’s all about</h6>
                            <h2>fitness first</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                nostrud.</p>
                            <a href="#">Read More</a>
                        </div>
                </div>
                <div className="love-your clearfix">
                    <img src={photoTwo} alt="watch" className="hand-watch" />
                        <div className="your-body">
                            <h6>love your</h6>
                            <h2>your body</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis
                                nostrud.</p>
                            <a href="#">Read More</a>
                        </div>
                </div>
            </div>
        </section>
    );
};

export default Section;