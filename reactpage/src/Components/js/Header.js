import React from 'react';
import  logo from '../img/logo.png';
import Nav from './Navigation';

const Header = () => {
  return (
    <header className="header">
        <div className="container clearfix">
            <h1 className="logo">
                <a href="#">
                    <img src={logo} alt="logo" />
                </a>
            </h1>
            <Nav/>
        </div>
    </header>
  );
};

export default Header;